<!DOCTYPE html>
<html lang="en"> 
<head>
    <title>Principal's Dashboard</title>
    
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <meta name="description" content="Portal - Bootstrap 5 Admin Dashboard Template For Developers">
    <meta name="author" content="Xiaoying Riley at 3rd Wave Media">    
    <link rel="shortcut icon" href="favicon.ico"> 
    
    <!-- FontAwesome JS-->
    <script defer src="{{ asset('portaldb/assets/plugins/fontawesome/js/all.min.js') }}"></script>
    
    <!-- App CSS -->  
    <link id="theme-style" rel="stylesheet" href="{{ asset('portaldb/assets/css/portal.css') }}">

</head> 

<body class="app">   	

    <x-backend.principal.layouts.partials.sidebar />
    
    <div class="app-wrapper">
	    
	{{ $slot }}
	    
	<x-backend.principal.layouts.partials.footer />
	    
    </div><!--//app-wrapper-->    					

 
    <!-- Javascript -->          
    <script src="{{ asset('portaldb/assets/plugins/popper.min.js') }}"></script>
    <script src="{{ asset('portaldb/assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>  

    <!-- Charts JS -->
    <script src="{{ asset('portaldb/assets/plugins/chart.js/chart.min.js') }}"></script> 
    <script src="{{ asset('portaldb/assets/js/index-charts.js') }}"></script> 
    
    <!-- Page Specific JS -->
    <script src="{{ asset('portaldb/assets/js/app.js') }}"></script> 

</body>
</html> 

