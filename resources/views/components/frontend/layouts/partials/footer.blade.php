<footer class="footer mt-auto py-3 bg-dark">
  <div class="container">
    <span class="text-white">
      <table>
        <thead>
          <th class="col-md-4">ABOUT</th>
          <th class="col-md-4">CONTACT</th>
          <th class="col-md-4">COMMUNITY CLUBS</th>
        </thead>

        <tbody>
          <td class="col-md-4">JJ School and College is aspiring to be the best educational institute.</td>
          <td class="col-md-4">Email : JJschool&college@gmail.com <br> Phone : +889 365 1247 <br> You can contact us 24/7, we are always ready to help.</td>
          <td class="col-md-4">Check out our community clubs to engage in various activites besides the regular curriculum.</td>
        </tbody>
      </table>
    </span>
  </div>
</footer>