<x-backend.admin.layouts.master>
<div class="container-fluid px-4">
                        <h1 class="mt-4">{{ $teacher->name }}</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                            <li class="breadcrumb-item active">teachers</li>
                        </ol>
                        
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                                Edit Teacher information
                                <a class="btn btn-sm btn-primary" href="{{ route('teachers.index')}}"> Teacher List</a>
                            </div>
                            <div class="card-body">

   @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('teachers.update', ['teacher' => $teacher->id])}}" method="POST">
    @csrf
      @method('PATCH')          

                <div class="mb-3">
                    <label for="title" class="form-label">Subject</label>
                    <input name="subject" type="text" class="form-control" id="subject" value="{{old('subject', $teacher->subject)}}">
                    
                    @error('subject')
                    <div  class="text-danger">{{ $message }}</div>
                    @enderror
                </div>


                <div class="mb-3">
                    <label for="name" class="form-label">Name</label>
                    <input name="name" type="text" class="form-control" id="name" value="{{old('name', $teacher->name)}}" >
                    
                    @error('name')
                    <div  class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="contact" class="form-label">Contact</label>
                    <input name="contact" type="number" class="form-control" id="contact" value="{{old('number', $teacher->contact)}}" >
                    
                    @error('contact')
                    <div  class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="email" class="form-label">Email</label>
                    <input name="email" type="email" class="form-control" id="email" value="{{old('email', $teacher->email)}}" >
                    
                    @error('email')
                    <div  class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="present_address" class="form-label">Present Address</label>
                    <input name="present_address" type="varchar" class="form-control" id="present_address" value="{{old('present_address', $teacher->present_address)}}" >
                    
                    @error('present_address')
                    <div  class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="permanent_address" class="form-label">Permanent Address </label>
                    <input name="permanent_address" type="varchar" class="form-control" id="permanent_address" value="{{old('permanent_address', $teacher->permanent_address)}}" >
                    
                    @error('permanent_address')
                    <div  class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="designation" class="form-label">Designation</label>
                    <input name="designation" type="text" class="form-control" id="designation" value="{{old('designation', $teacher->designation)}}" >
                    
                    @error('designation')
                    <div  class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="educational_qualification" class="form-label">Educational Qualification</label>
                    <input name="educational_qualification" type="text" class="form-control" id="educational_qualification" value="{{old('educational_qualification', $teacher->educational_qualification)}}" >
                    
                    @error('educational_qualification')
                    <div  class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="joining_date" class="form-label">Joining Date</label>
                    <input name="joining_date" type="date" class="form-control" id="joining_date" value="{{old('joining_date', $teacher->joining_date)}}" >
                    
                    @error('joining_date')
                    <div  class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
  
                <button type="submit" class="btn btn-primary">Save</button>
</form>
                            </div>
                        </div>
                   
</x-backend.admin.layouts.master>