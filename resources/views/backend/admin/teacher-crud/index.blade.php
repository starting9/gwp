<x-backend.admin.layouts.master>
<div class="container-fluid px-4">
                        <h1 class="mt-4">Teachers</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item">Teachers</li>
                            <li class="breadcrumb-item active">teachers</li>
                        </ol>
                        
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                                Teacher List
                                <a class="btn btn-sm btn-primary" href="{{ route('teachers.create')}}">Register New Teacher </a>
                            </div>
                            <div class="card-body">
                                @if(session('message'))
                                <p class="alert alert-success">{{ session('message') }}</p>
                                @endif
                                <table id="datatablesSimple" style="width : 100%">
                                    <thead>
                                        <tr>
                                            <th scope="col">ID</th>
                                            <th>Subject</th>
                                            <th>Name</th>
                                            <th>Contact </th>
                                            <th>Email</th>
                                            <th>Designation</th>
                                            <th>Action</th>
                                          
                                        </tr>
                                    </thead>
                                    
    <tbody>
        @foreach ($teachers as $teacher)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $teacher->subject }}</td>
            <td>{{ $teacher->name }} </td>
            <td>{{ $teacher->contact }} </td>
            <td>{{ $teacher->email }} </td>
            <!-- <td>{{ $teacher->present_address }} </td>
            <td>{{ $teacher->permanent_address }} </td> -->
            <td>{{ $teacher->designation }} </td>
            <!-- <td>{{ $teacher->educational_qualification }} </td>
            <td>{{ $teacher->joining_date }} </td> -->
            <td>
            <a class="btn btn-warning btn-sm" href="{{route('teachers.show', ['teacher' => $teacher->id]) }}"> Show </a>
                
                <a class="btn btn-warning btn-sm" href="{{route('teachers.edit', ['teacher' => $teacher->id]) }}"> Edit </a>
                
                <form action="{{ route('teachers.destroy', ['teacher' => $teacher->id]) }}" method="POST" style="display:inline">
                   @csrf
                   @method('delete') 
                   <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete?')">Delete</button>
                </form>
               
            
            </td>
            
        </tr>
        @endforeach
    </tbody>
</table>
                            </div>
                        </div>
                    </div>
</x-backend.admin.layouts.master>