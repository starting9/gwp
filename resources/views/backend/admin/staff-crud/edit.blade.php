<x-backend.admin.layouts.master>
<div class="container-fluid px-4">
                        <h1 class="mt-4">{{ $staff->name }}</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                            <li class="breadcrumb-item active">staffs</li>
                        </ol>
                        
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                                Edit Staff Information
                                <a class="btn btn-sm btn-primary" href="{{ route('staffs.index')}}"> Staff List</a>
                            </div>
                            <div class="card-body">

   @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('staffs.update', ['staff' => $staff->id])}}" method="POST">
    @csrf
      @method('PATCH')          

                


                <div class="mb-3">
                    <label for="name" class="form-label">Name</label>
                    <input name="name" type="text" class="form-control" id="name" value="{{old('name', $staff->name)}}" >
                    
                    @error('name')
                    <div  class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="contact" class="form-label">Contact</label>
                    <input name="contact" type="number" class="form-control" id="contact" value="{{old('number', $staff->contact)}}" >
                    
                    @error('contact')
                    <div  class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="email" class="form-label">Email</label>
                    <input name="email" type="email" class="form-control" id="email" value="{{old('email', $staff->email)}}" >
                    
                    @error('email')
                    <div  class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="present_address" class="form-label">Present Address</label>
                    <input name="present_address" type="varchar" class="form-control" id="present_address" value="{{old('present_address', $staff->present_address)}}" >
                    
                    @error('present_address')
                    <div  class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="permanent_address" class="form-label">Permanent Address </label>
                    <input name="permanent_address" type="varchar" class="form-control" id="permanent_address" value="{{old('permanent_address', $staff->permanent_address)}}" >
                    
                    @error('permanent_address')
                    <div  class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="designation" class="form-label">Designation</label>
                    <input name="designation" type="text" class="form-control" id="designation" value="{{old('designation', $staff->designation)}}" >
                    
                    @error('designation')
                    <div  class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="educational_qualification" class="form-label">Educational Qualification</label>
                    <input name="educational_qualification" type="text" class="form-control" id="educational_qualification" value="{{old('educational_qualification', $staff->educational_qualification)}}" >
                    
                    @error('educational_qualification')
                    <div  class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="joining_date" class="form-label">Joining Date</label>
                    <input name="joining_date" type="date" class="form-control" id="joining_date" value="{{old('joining_date', $staff->joining_date)}}" >
                    
                    @error('joining_date')
                    <div  class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
  
                <button type="submit" class="btn btn-primary">Save</button>
</form>
                            </div>
                        </div>
                   
</x-backend.admin.layouts.master>