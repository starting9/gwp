<x-backend.admin.layouts.master>
<div class="container-fluid px-4">
                        <h1 class="mt-4">Staffs</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                            <li class="breadcrumb-item active">Staffs</li>
                        </ol>
                        
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                                Staff Details
                                <a class="btn btn-sm btn-primary" href="{{ route('staffs.index')}}">Staff List </a>
                            </div>
                            <div class="card-body">

                               
                               <h3> Name: {{$staff->name}}</h3>
                               
                                <p> Contact: {{ $staff->contact }} </p>
                               <p> Email: {{ $staff->email }} </p>
                               <p> Present Address: {{ $staff->present_address }} </p>
                               <p> Permanent Address: {{ $staff->permanent_address }} </p>
                               <p> Designation: {{ $staff->designation }} </p>
                               <p> Educational Qualification: {{ $staff->educational_qualification }} </p>
                               <p> Joining Date: {{ $staff->joining_date }} </p>
                              
                            </div>
                        </div>
                    </div>
</x-backend.admin.layouts.master>