<x-backend.admin.layouts.master>
<div class="container-fluid px-4">
                        <h1 class="mt-4">Staffs</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                            <li class="breadcrumb-item active">staffs</li>
                        </ol>
                        
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                                Staff List
                                <a class="btn btn-sm btn-primary" href="{{ route('staffs.create')}}">Register New Staff </a>
                            </div>
                            <div class="card-body">
                                @if(session('message'))
                                <p class="alert alert-success">{{ session('message') }}</p>
                                @endif
                                <div class="col-md-12">
                                <table id="datatablesSimple" style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Designation</th>
                                            <th>Contact </th>
                                            <th>Email</th>
                                            <!-- <th>Present Address</th>
                                            <th>Permanent Address</th> -->

                                            <!-- <th>Educational Qualification</th>
                                            <th>Joining Date</th> -->
        
                                            <th>Action</th>
                                          
                                        </tr>
                                    </thead>
                                    
    <tbody>
        @foreach ($staffs as $staff)
        <tr>
            <td>{{ $loop->iteration }}</td>
            
            <td>{{ $staff->name }} </td>
             <td>{{ $staff->designation }} </td>
            <td>{{ $staff->contact }} </td>
            <td>{{ $staff->email }} </td>
            
            <td>
            <a class="btn btn-warning btn-sm" href="{{route('staffs.show', ['staff' => $staff->id]) }}"> Show </a>
                
                <a class="btn btn-warning btn-sm" href="{{route('staffs.edit', ['staff' => $staff->id]) }}"> Edit </a>
                
                <form action="{{ route('staffs.destroy', ['staff' => $staff->id]) }}" method="POST" style="display:inline">
                   @csrf
                   @method('delete') 
                   <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete?')">Delete</button>
                </form>
               
            
            </td>
            
        </tr>
        @endforeach
    </tbody>
</table>
</div>
                            </div>
                        </div>
                    </div>
</x-backend.admin.layouts.master>