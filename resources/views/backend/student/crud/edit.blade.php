<x-backend.student.layouts.master>
<div class="container">
<form action="{{route('students.update',['student'=>$student->id])}}" method="Post">

@csrf
@method('patch')

    
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="row" style="margin-top:50px; margin-bottom: 10px;">
        <div class="col">
          <label>Student Name:</label>
          <input  value="{{$student->student_name}}" name="student_name" type="text" class="form-control" placeholder="Enter full name" aria-label="First name">
        </div>
        <div class="col">

          <label>Student Class  </label>
          <input value="{{$student->student_class}}"name="student_class"type="text" class="form-control" placeholder="student class" >

        </div>
      </div>
      <div class="row" style="margin-bottom: 10px;">
        <div class="col">
          <label for=""> email:</label>
          <input value="{{$student->email}}" name="email" type="email" class="form-control" >
        </div>
        <div class="col">
          <label>Password:</label>
          <input value="{{$student->password}}" name="password"type="password" class="form-control" aria-label="password">
        </div>
      </div>
      <div class="row" style="margin-bottom: 10px;">
        <div class="col">
          <label for="">Father Name</label>
          <input value="{{$student->father_name}}" name="father_name" type="text" class="form-control" aria-label="First name">
        </div>
        <div class="col">
          <label for="phone"> phone number</label>
          <input  value="{{$student->father_phone}}"name="father_phone" type="number" class="form-control" placeholder="(+880)" aria-label="Last name">
        </div>
      </div>
      <div class="row" style="margin-bottom: 10px;">
        <div class="col">
          <label for="">Mother Name</label>
          <input value="{{$student->mother_name}}" name="mother_name"type="text" class="form-control" >
        </div>
        <div class="col">
          <label for="phone"> phone number</label>
          <input value="{{$student->mother_phone}}" name="mother_phone" type="number" class="form-control" placeholder="(+880)" >
        </div>
      </div>

       <div class="row" style="margin: bottom 10px;">
      
      <div class="col">
          <label for="dateofbirth">Date of birth:</label>
          <div class="row">
            <input value="{{$student->date_of_birth}}" name="date_of_birth" type="date">
          </div>
      </div>
        <div class="col">
          <label for="gender">Gender:</label>
          <select name="gender" class="form-select" aria-label="Default select example" id="gender">
            <option selected>Select your Gender</option>
            <option value="male">Male</option>
            <option value="female">Female</option>
          </select>
        </div>
</div>

     
      <!-- <div class="row" style="margin-bottom: 10px;"> -->
        <div class="row ">
          <label for="address">Address:</label>
          <div class="row">
          
              <textarea value="{{$student->address}}" name="address" id="" cols="50" rows="2"></textarea></div>
        
        </div>
      <div class="mb-3">
  <label for="formFile" class="form-label">Student Picture:</label>
  <input class="form-control" type="file" id="image">
</div>
      <div class="row justify-content-center">
        <div class="col-auto ">
          <button type="submit" class="btn btn-primary"
            style="width: 200px; font-weight: bold; font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif; margin-bottom: 50px; margin-top: 20px;">Submit</button>
  
        </div>
        <br>
      </div>
   
      
    



    </form>
</div>
</x-backend.student.layouts.master>