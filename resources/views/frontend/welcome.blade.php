<x-frontend.layouts.master>
    
<div class="container">

<div class="row">
      <div class="col-lg-4">
        <h2>Student Registration Ongoing!</h2>
        <p>The May/June session is starting soon and students can still register for it. 
          Please register before 31st May 2022. Download the registration form linked above and fill it with 
          relevant information and submit the form in our office.</p>
        <p><a class="btn btn-secondary" href="{{route('student.pdf')}}">Student Registration Form</a></p>
      </div><!-- /.col-lg-4 -->

      <div class="col-lg-4">
        <h2>We are looking for Teachers!</h2>
        <p>Graduates can apply to be a Teacher in our school by 31st May 2022. 
          Further details will be given when you apply. 
          Please download the registration form from the top and fill it with relevant information 
          to submit the form in our office for an interview.</p>
        <p><a class="btn btn-secondary" href="{{route('teacher.pdf')}}">Teacher Registration Form</a></p>
      </div><!-- /.col-lg-4 -->
      
      <div class="col-lg-4">
        <h2>We've opened a Basketball Court!!</h2>
        <p>We've recently finished building our basketball court. 
          Now students also have an oppurtunity to try for professional Basketball teams in the future.</p>
        <p><a class="btn btn-secondary" href="#">Details</a></p>
      </div><!-- /.col-lg-4 -->
  </div><!-- /.row -->

  

  <!-- START THE FEATURETTES -->

  <hr class="featurette-divider">

<div class="row featurette">
  <div class="col-md-7">
    <h2 class="featurette-heading">Principal Raymond, <span class="text-muted">Announcement</span></h2>
    <p class="lead">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Repudiandae quaerat laudantium cum eaque aspernatur enim fuga error reiciendis, consequatur hic laboriosam, itaque rerum? Quos libero odio voluptas amet perspiciatis eos, quaerat laudantium necessitatibus quasi deserunt nulla nostrum mollitia nisi cum unde excepturi molestias voluptatum facere dolorum. Sequi exercitationem quam iste?</p>
  </div>
  <div class="col-md-5">
    <img class="bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto" width="500" height="500" src="{{ asset('img/avatar1.jpg') }}" />
  </div>
</div>

<hr class="featurette-divider">

<div class="row featurette">
  <div class="col-md-7 order-md-2">
    <h2 class="featurette-heading">Vice Principal Jane, <span class="text-muted">Announcement</span></h2>
    <p class="lead">Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus laborum architecto accusamus nemo aspernatur veritatis?Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ea blanditiis quod a corporis nam officia at beatae nulla. Enim, debitis.</p>
  </div>
  <div class="col-md-5 order-md-1">
  <img class="bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto" width="500" height="500" src="{{ asset('img/avatar2.png') }}" />
  </div>
</div>

<hr class="featurette-divider">

<div class="row featurette">
  <div class="col-md-7">
    <h2 class="featurette-heading">Career Advisor Andrew <span class="text-muted">Announcement</span></h2>
    <p class="lead">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Fuga voluptates ducimus sapiente aperiam dicta beatae hic quaerat voluptas voluptatibus amet.Lorem ipsum dolor sit amet consectetur, adipisicing elit. Totam ut qui reiciendis natus veritatis nulla fugit soluta illum unde molestiae.</p>
  </div>
  <div class="col-md-5">
  <img class="bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto" width="500" height="500" src="{{ asset('img/avatar1.jpg') }}" />
  </div>
</div>

<hr class="featurette-divider">

<!-- END THE FEATURETTES -->



</div> <!-- Container -->
</x-frontend.layouts.master>