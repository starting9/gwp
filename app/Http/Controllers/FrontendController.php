<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;


class FrontendController extends Controller
{
    public function index()
    {
        return view('frontend.welcome');
    }

    public function studentpdf()
    {
        $pdf = PDF::loadView('frontend.studentform');
        return $pdf->download('Registration.pdf');
    }

    public function teacherpdf()
    {
        $pdf = PDF::loadView('frontend.teacherform');
        return $pdf->download('Employment.pdf');
    }
}
