<?php

namespace App\Http\Controllers;

use App\Models\Teacher;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teachers = Teacher::orderBy('id', 'desc')->get();
        return view('backend.admin.teacher-crud.index', compact('teachers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.admin.teacher-crud.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dd($request->all());
        try{
            Teacher::create($request->all());   
 
            return redirect()->route('teachers.index')->withMessage('Successfully Saved !');

        } catch(QueryException $e){
             return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $teacher = Teacher::findOrFail($id);
        return view('backend.admin.teacher-crud.show', compact('teacher'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Teacher $teacher)
    {
        return view('backend.admin.teacher-crud.edit', compact('teacher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{ 
            $teacher = Teacher::findOrFail($id);
                $teacher->update([
                    'subject' => $request->subject,
                    'name' => $request->name,
                    'contact' => $request->contact,
                    'email' => $request->email,
                    'present_address' => $request->present_address,
                    'parmanent_address' => $request->permanent_address,
                    'designation' => $request->designation,
                    'educational_qualification' => $request->educational_qualification,
                ]);
                
   return redirect()->route('teachers.index')->withMessage('Successfully Updated!');

       }catch (QueryException $e){
           return redirect()->back()->withInput()->withErrors($e->getMessage());

       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Teacher $teacher)
    {
        $teacher->delete();
        return redirect()->route('teachers.index')->withMessage('Successfully Deleted !');
    }
}
