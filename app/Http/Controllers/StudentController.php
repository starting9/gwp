<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::orderBy('id','desc')->get();
        
        return view('backend.student.crud.index',compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.student.crud.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=$request->all();
       
        try 
        {
            Student::create(
                [
                'student_name'=>$data['student_name'],
                'student_class'=>$data['student_class'],
                'email'=>$data['email'],
                'password'=>$data['password'],
                'father_name'=>$data['father_name'],
                'father_phone'=>$data['father_phone'],
                'mother_name'=>$data['mother_name'],
                'mother_phone'=>$data['mother_phone'],
                'date_of_birth'=>$data['date_of_birth'],
                'gender'=>$data['gender'],
                'address'=>$data['address']
                ]);

                return redirect()->route('students.index')->withMessage('Successfully Saved !');

        } catch (QueryException $e)
        {
            // dd($ex);
        }
    
       
            // dd($request);

            return redirect()->route('backend.student.crud.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        return view('backend.student.crud.show',compact('student'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        return view('backend.student.crud.edit', compact('student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        $data=$request->all();

        $student->update([

            'student_name'=>$data['student_name'],
              'student_class'=>$data['student_class'],
              'email'=>$data['email'],
              'password'=>$data['password'],
              'father_name'=>$data['father_name'],
              'father_phone'=>$data['father_phone'],
               'mother_name'=>$data['mother_name'],
              'mother_phone'=>$data['mother_phone'],
              'date_of_birth'=>$data['date_of_birth'],
              'gender'=>$data['gender'],
                'address'=>$data['address']

        ]);
        return redirect()->route('students.index')->withMessage('Successfully Update!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        $student->delete();
        return redirect()->route('students.index')->withMessage('Successfully Deleted !');
    }
}
