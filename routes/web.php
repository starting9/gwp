<?php

use App\Http\Controllers\DealController;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\StaffController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\TeacherController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FrontendController::class, 'index'])->name('welcome');
Route::get('/register-form', [FrontendController::class, 'studentpdf'])->name('student.pdf');
Route::get('/employment-form', [FrontendController::class, 'teacherpdf'])->name('teacher.pdf');



Route::get('/dashboard/admin', function () {
    return view('backend.admin.index');
})->middleware(['auth'])->name('dashboard.admin');

    Route::resource('teachers', TeacherController::class);
    Route::resource('staffs', StaffController::class);

Route::get('/dashboard/principal', function () {
    return view('backend.principal.index');
})->middleware(['auth'])->name('dashboard.principal');

    Route::resource('deals', DealController::class);





Route::get('/dashboard/parent', function () {
    return view('backend.parent.index');
})->middleware(['auth'])->name('dashboard.parent');



Route::get('/dashboard/teacher', function () {
    return view('backend.teacher.index');
})->middleware(['auth'])->name('dashboard.teacher');

    Route::resource('teachers', TeacherController::class);


Route::get('/dashboard/student', function () {
    return view('backend.student.index');
})->middleware(['auth'])->name('dashboard.student');

    Route::resource('students', StudentController::class);

require __DIR__.'/auth.php';
